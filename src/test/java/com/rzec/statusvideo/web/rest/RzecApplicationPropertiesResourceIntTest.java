package com.rzec.statusvideo.web.rest;

import com.rzec.statusvideo.StatusVideoApp;

import com.rzec.statusvideo.domain.RzecApplicationProperties;
import com.rzec.statusvideo.repository.RzecApplicationPropertiesRepository;
import com.rzec.statusvideo.service.RzecApplicationPropertiesService;
import com.rzec.statusvideo.service.dto.RzecApplicationPropertiesDTO;
import com.rzec.statusvideo.service.mapper.RzecApplicationPropertiesMapper;
import com.rzec.statusvideo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.rzec.statusvideo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RzecApplicationPropertiesResource REST controller.
 *
 * @see RzecApplicationPropertiesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StatusVideoApp.class)
public class RzecApplicationPropertiesResourceIntTest {

    private static final String DEFAULT_PROPERTY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PROPERTY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Autowired
    private RzecApplicationPropertiesRepository rzecApplicationPropertiesRepository;

    @Autowired
    private RzecApplicationPropertiesMapper rzecApplicationPropertiesMapper;

    @Autowired
    private RzecApplicationPropertiesService rzecApplicationPropertiesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restRzecApplicationPropertiesMockMvc;

    private RzecApplicationProperties rzecApplicationProperties;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RzecApplicationPropertiesResource rzecApplicationPropertiesResource = new RzecApplicationPropertiesResource(rzecApplicationPropertiesService);
        this.restRzecApplicationPropertiesMockMvc = MockMvcBuilders.standaloneSetup(rzecApplicationPropertiesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RzecApplicationProperties createEntity() {
        RzecApplicationProperties rzecApplicationProperties = new RzecApplicationProperties()
            .propertyName(DEFAULT_PROPERTY_NAME)
            .description(DEFAULT_DESCRIPTION)
            .value(DEFAULT_VALUE);
        return rzecApplicationProperties;
    }

    @Before
    public void initTest() {
        rzecApplicationPropertiesRepository.deleteAll();
        rzecApplicationProperties = createEntity();
    }

    @Test
    public void createRzecApplicationProperties() throws Exception {
        int databaseSizeBeforeCreate = rzecApplicationPropertiesRepository.findAll().size();

        // Create the RzecApplicationProperties
        RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO = rzecApplicationPropertiesMapper.toDto(rzecApplicationProperties);
        restRzecApplicationPropertiesMockMvc.perform(post("/api/rzec-application-properties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rzecApplicationPropertiesDTO)))
            .andExpect(status().isCreated());

        // Validate the RzecApplicationProperties in the database
        List<RzecApplicationProperties> rzecApplicationPropertiesList = rzecApplicationPropertiesRepository.findAll();
        assertThat(rzecApplicationPropertiesList).hasSize(databaseSizeBeforeCreate + 1);
        RzecApplicationProperties testRzecApplicationProperties = rzecApplicationPropertiesList.get(rzecApplicationPropertiesList.size() - 1);
        assertThat(testRzecApplicationProperties.getPropertyName()).isEqualTo(DEFAULT_PROPERTY_NAME);
        assertThat(testRzecApplicationProperties.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRzecApplicationProperties.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    public void createRzecApplicationPropertiesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = rzecApplicationPropertiesRepository.findAll().size();

        // Create the RzecApplicationProperties with an existing ID
        rzecApplicationProperties.setId("existing_id");
        RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO = rzecApplicationPropertiesMapper.toDto(rzecApplicationProperties);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRzecApplicationPropertiesMockMvc.perform(post("/api/rzec-application-properties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rzecApplicationPropertiesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RzecApplicationProperties in the database
        List<RzecApplicationProperties> rzecApplicationPropertiesList = rzecApplicationPropertiesRepository.findAll();
        assertThat(rzecApplicationPropertiesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllRzecApplicationProperties() throws Exception {
        // Initialize the database
        rzecApplicationPropertiesRepository.save(rzecApplicationProperties);

        // Get all the rzecApplicationPropertiesList
        restRzecApplicationPropertiesMockMvc.perform(get("/api/rzec-application-properties?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rzecApplicationProperties.getId())))
            .andExpect(jsonPath("$.[*].propertyName").value(hasItem(DEFAULT_PROPERTY_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    public void getRzecApplicationProperties() throws Exception {
        // Initialize the database
        rzecApplicationPropertiesRepository.save(rzecApplicationProperties);

        // Get the rzecApplicationProperties
        restRzecApplicationPropertiesMockMvc.perform(get("/api/rzec-application-properties/{id}", rzecApplicationProperties.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(rzecApplicationProperties.getId()))
            .andExpect(jsonPath("$.propertyName").value(DEFAULT_PROPERTY_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    public void getNonExistingRzecApplicationProperties() throws Exception {
        // Get the rzecApplicationProperties
        restRzecApplicationPropertiesMockMvc.perform(get("/api/rzec-application-properties/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateRzecApplicationProperties() throws Exception {
        // Initialize the database
        rzecApplicationPropertiesRepository.save(rzecApplicationProperties);
        int databaseSizeBeforeUpdate = rzecApplicationPropertiesRepository.findAll().size();

        // Update the rzecApplicationProperties
        RzecApplicationProperties updatedRzecApplicationProperties = rzecApplicationPropertiesRepository.findOne(rzecApplicationProperties.getId());
        updatedRzecApplicationProperties
            .propertyName(UPDATED_PROPERTY_NAME)
            .description(UPDATED_DESCRIPTION)
            .value(UPDATED_VALUE);
        RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO = rzecApplicationPropertiesMapper.toDto(updatedRzecApplicationProperties);

        restRzecApplicationPropertiesMockMvc.perform(put("/api/rzec-application-properties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rzecApplicationPropertiesDTO)))
            .andExpect(status().isOk());

        // Validate the RzecApplicationProperties in the database
        List<RzecApplicationProperties> rzecApplicationPropertiesList = rzecApplicationPropertiesRepository.findAll();
        assertThat(rzecApplicationPropertiesList).hasSize(databaseSizeBeforeUpdate);
        RzecApplicationProperties testRzecApplicationProperties = rzecApplicationPropertiesList.get(rzecApplicationPropertiesList.size() - 1);
        assertThat(testRzecApplicationProperties.getPropertyName()).isEqualTo(UPDATED_PROPERTY_NAME);
        assertThat(testRzecApplicationProperties.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRzecApplicationProperties.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    public void updateNonExistingRzecApplicationProperties() throws Exception {
        int databaseSizeBeforeUpdate = rzecApplicationPropertiesRepository.findAll().size();

        // Create the RzecApplicationProperties
        RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO = rzecApplicationPropertiesMapper.toDto(rzecApplicationProperties);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRzecApplicationPropertiesMockMvc.perform(put("/api/rzec-application-properties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rzecApplicationPropertiesDTO)))
            .andExpect(status().isCreated());

        // Validate the RzecApplicationProperties in the database
        List<RzecApplicationProperties> rzecApplicationPropertiesList = rzecApplicationPropertiesRepository.findAll();
        assertThat(rzecApplicationPropertiesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteRzecApplicationProperties() throws Exception {
        // Initialize the database
        rzecApplicationPropertiesRepository.save(rzecApplicationProperties);
        int databaseSizeBeforeDelete = rzecApplicationPropertiesRepository.findAll().size();

        // Get the rzecApplicationProperties
        restRzecApplicationPropertiesMockMvc.perform(delete("/api/rzec-application-properties/{id}", rzecApplicationProperties.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RzecApplicationProperties> rzecApplicationPropertiesList = rzecApplicationPropertiesRepository.findAll();
        assertThat(rzecApplicationPropertiesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RzecApplicationProperties.class);
        RzecApplicationProperties rzecApplicationProperties1 = new RzecApplicationProperties();
        rzecApplicationProperties1.setId("id1");
        RzecApplicationProperties rzecApplicationProperties2 = new RzecApplicationProperties();
        rzecApplicationProperties2.setId(rzecApplicationProperties1.getId());
        assertThat(rzecApplicationProperties1).isEqualTo(rzecApplicationProperties2);
        rzecApplicationProperties2.setId("id2");
        assertThat(rzecApplicationProperties1).isNotEqualTo(rzecApplicationProperties2);
        rzecApplicationProperties1.setId(null);
        assertThat(rzecApplicationProperties1).isNotEqualTo(rzecApplicationProperties2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RzecApplicationPropertiesDTO.class);
        RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO1 = new RzecApplicationPropertiesDTO();
        rzecApplicationPropertiesDTO1.setId("id1");
        RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO2 = new RzecApplicationPropertiesDTO();
        assertThat(rzecApplicationPropertiesDTO1).isNotEqualTo(rzecApplicationPropertiesDTO2);
        rzecApplicationPropertiesDTO2.setId(rzecApplicationPropertiesDTO1.getId());
        assertThat(rzecApplicationPropertiesDTO1).isEqualTo(rzecApplicationPropertiesDTO2);
        rzecApplicationPropertiesDTO2.setId("id2");
        assertThat(rzecApplicationPropertiesDTO1).isNotEqualTo(rzecApplicationPropertiesDTO2);
        rzecApplicationPropertiesDTO1.setId(null);
        assertThat(rzecApplicationPropertiesDTO1).isNotEqualTo(rzecApplicationPropertiesDTO2);
    }
}

package com.rzec.statusvideo.web.rest;

import com.rzec.statusvideo.StatusVideoApp;

import com.rzec.statusvideo.domain.Videos;
import com.rzec.statusvideo.repository.VideosRepository;
import com.rzec.statusvideo.service.VideosService;
import com.rzec.statusvideo.service.dto.VideosDTO;
import com.rzec.statusvideo.service.mapper.VideosMapper;
import com.rzec.statusvideo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.rzec.statusvideo.web.rest.TestUtil.sameInstant;
import static com.rzec.statusvideo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.rzec.statusvideo.domain.enumeration.Language;
/**
 * Test class for the VideosResource REST controller.
 *
 * @see VideosResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StatusVideoApp.class)
public class VideosResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final Integer DEFAULT_VIEWS = 1;
    private static final Integer UPDATED_VIEWS = 2;

    private static final String DEFAULT_TAG = "AAAAAAAAAA";
    private static final String UPDATED_TAG = "BBBBBBBBBB";

    private static final String DEFAULT_ACTOR = "AAAAAAAAAA";
    private static final String UPDATED_ACTOR = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_GENRE = "AAAAAAAAAA";
    private static final String UPDATED_GENRE = "BBBBBBBBBB";

    private static final Language DEFAULT_LANGUAGE = Language.TAMIL;
    private static final Language UPDATED_LANGUAGE = Language.HINDI;

    private static final String DEFAULT_CREDITS = "AAAAAAAAAA";
    private static final String UPDATED_CREDITS = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE_URL = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_URL = "BBBBBBBBBB";

    private static final String DEFAULT_MOVIE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MOVIE_NAME = "BBBBBBBBBB";

    @Autowired
    private VideosRepository videosRepository;

    @Autowired
    private VideosMapper videosMapper;

    @Autowired
    private VideosService videosService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restVideosMockMvc;

    private Videos videos;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VideosResource videosResource = new VideosResource(videosService);
        this.restVideosMockMvc = MockMvcBuilders.standaloneSetup(videosResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Videos createEntity() {
        Videos videos = new Videos()
            .name(DEFAULT_NAME)
            .url(DEFAULT_URL)
            .views(DEFAULT_VIEWS)
            .tag(DEFAULT_TAG)
            .actor(DEFAULT_ACTOR)
            .createdDate(DEFAULT_CREATED_DATE)
            .genre(DEFAULT_GENRE)
            .language(DEFAULT_LANGUAGE)
            .credits(DEFAULT_CREDITS)
            .image_url(DEFAULT_IMAGE_URL)
            .movie_name(DEFAULT_MOVIE_NAME);
        return videos;
    }

    @Before
    public void initTest() {
        videosRepository.deleteAll();
        videos = createEntity();
    }

    @Test
    public void createVideos() throws Exception {
        int databaseSizeBeforeCreate = videosRepository.findAll().size();

        // Create the Videos
        VideosDTO videosDTO = videosMapper.toDto(videos);
        restVideosMockMvc.perform(post("/api/videos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(videosDTO)))
            .andExpect(status().isCreated());

        // Validate the Videos in the database
        List<Videos> videosList = videosRepository.findAll();
        assertThat(videosList).hasSize(databaseSizeBeforeCreate + 1);
        Videos testVideos = videosList.get(videosList.size() - 1);
        assertThat(testVideos.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVideos.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testVideos.getViews()).isEqualTo(DEFAULT_VIEWS);
        assertThat(testVideos.getTag()).isEqualTo(DEFAULT_TAG);
        assertThat(testVideos.getActor()).isEqualTo(DEFAULT_ACTOR);
        assertThat(testVideos.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testVideos.getGenre()).isEqualTo(DEFAULT_GENRE);
        assertThat(testVideos.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testVideos.getCredits()).isEqualTo(DEFAULT_CREDITS);
        assertThat(testVideos.getImage_url()).isEqualTo(DEFAULT_IMAGE_URL);
        assertThat(testVideos.getMovie_name()).isEqualTo(DEFAULT_MOVIE_NAME);
    }

    @Test
    public void createVideosWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = videosRepository.findAll().size();

        // Create the Videos with an existing ID
        videos.setId("existing_id");
        VideosDTO videosDTO = videosMapper.toDto(videos);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVideosMockMvc.perform(post("/api/videos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(videosDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Videos in the database
        List<Videos> videosList = videosRepository.findAll();
        assertThat(videosList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllVideos() throws Exception {
        // Initialize the database
        videosRepository.save(videos);

        // Get all the videosList
        restVideosMockMvc.perform(get("/api/videos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(videos.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].views").value(hasItem(DEFAULT_VIEWS)))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG.toString())))
            .andExpect(jsonPath("$.[*].actor").value(hasItem(DEFAULT_ACTOR.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].genre").value(hasItem(DEFAULT_GENRE.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
            .andExpect(jsonPath("$.[*].credits").value(hasItem(DEFAULT_CREDITS.toString())))
            .andExpect(jsonPath("$.[*].image_url").value(hasItem(DEFAULT_IMAGE_URL.toString())))
            .andExpect(jsonPath("$.[*].movie_name").value(hasItem(DEFAULT_MOVIE_NAME.toString())));
    }

    @Test
    public void getVideos() throws Exception {
        // Initialize the database
        videosRepository.save(videos);

        // Get the videos
        restVideosMockMvc.perform(get("/api/videos/{id}", videos.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(videos.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.views").value(DEFAULT_VIEWS))
            .andExpect(jsonPath("$.tag").value(DEFAULT_TAG.toString()))
            .andExpect(jsonPath("$.actor").value(DEFAULT_ACTOR.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.genre").value(DEFAULT_GENRE.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.credits").value(DEFAULT_CREDITS.toString()))
            .andExpect(jsonPath("$.image_url").value(DEFAULT_IMAGE_URL.toString()))
            .andExpect(jsonPath("$.movie_name").value(DEFAULT_MOVIE_NAME.toString()));
    }

    @Test
    public void getNonExistingVideos() throws Exception {
        // Get the videos
        restVideosMockMvc.perform(get("/api/videos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateVideos() throws Exception {
        // Initialize the database
        videosRepository.save(videos);
        int databaseSizeBeforeUpdate = videosRepository.findAll().size();

        // Update the videos
        Videos updatedVideos = videosRepository.findOne(videos.getId());
        updatedVideos
            .name(UPDATED_NAME)
            .url(UPDATED_URL)
            .views(UPDATED_VIEWS)
            .tag(UPDATED_TAG)
            .actor(UPDATED_ACTOR)
            .createdDate(UPDATED_CREATED_DATE)
            .genre(UPDATED_GENRE)
            .language(UPDATED_LANGUAGE)
            .credits(UPDATED_CREDITS)
            .image_url(UPDATED_IMAGE_URL)
            .movie_name(UPDATED_MOVIE_NAME);
        VideosDTO videosDTO = videosMapper.toDto(updatedVideos);

        restVideosMockMvc.perform(put("/api/videos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(videosDTO)))
            .andExpect(status().isOk());

        // Validate the Videos in the database
        List<Videos> videosList = videosRepository.findAll();
        assertThat(videosList).hasSize(databaseSizeBeforeUpdate);
        Videos testVideos = videosList.get(videosList.size() - 1);
        assertThat(testVideos.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVideos.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testVideos.getViews()).isEqualTo(UPDATED_VIEWS);
        assertThat(testVideos.getTag()).isEqualTo(UPDATED_TAG);
        assertThat(testVideos.getActor()).isEqualTo(UPDATED_ACTOR);
        assertThat(testVideos.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testVideos.getGenre()).isEqualTo(UPDATED_GENRE);
        assertThat(testVideos.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testVideos.getCredits()).isEqualTo(UPDATED_CREDITS);
        assertThat(testVideos.getImage_url()).isEqualTo(UPDATED_IMAGE_URL);
        assertThat(testVideos.getMovie_name()).isEqualTo(UPDATED_MOVIE_NAME);
    }

    @Test
    public void updateNonExistingVideos() throws Exception {
        int databaseSizeBeforeUpdate = videosRepository.findAll().size();

        // Create the Videos
        VideosDTO videosDTO = videosMapper.toDto(videos);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVideosMockMvc.perform(put("/api/videos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(videosDTO)))
            .andExpect(status().isCreated());

        // Validate the Videos in the database
        List<Videos> videosList = videosRepository.findAll();
        assertThat(videosList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteVideos() throws Exception {
        // Initialize the database
        videosRepository.save(videos);
        int databaseSizeBeforeDelete = videosRepository.findAll().size();

        // Get the videos
        restVideosMockMvc.perform(delete("/api/videos/{id}", videos.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Videos> videosList = videosRepository.findAll();
        assertThat(videosList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Videos.class);
        Videos videos1 = new Videos();
        videos1.setId("id1");
        Videos videos2 = new Videos();
        videos2.setId(videos1.getId());
        assertThat(videos1).isEqualTo(videos2);
        videos2.setId("id2");
        assertThat(videos1).isNotEqualTo(videos2);
        videos1.setId(null);
        assertThat(videos1).isNotEqualTo(videos2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VideosDTO.class);
        VideosDTO videosDTO1 = new VideosDTO();
        videosDTO1.setId("id1");
        VideosDTO videosDTO2 = new VideosDTO();
        assertThat(videosDTO1).isNotEqualTo(videosDTO2);
        videosDTO2.setId(videosDTO1.getId());
        assertThat(videosDTO1).isEqualTo(videosDTO2);
        videosDTO2.setId("id2");
        assertThat(videosDTO1).isNotEqualTo(videosDTO2);
        videosDTO1.setId(null);
        assertThat(videosDTO1).isNotEqualTo(videosDTO2);
    }
}

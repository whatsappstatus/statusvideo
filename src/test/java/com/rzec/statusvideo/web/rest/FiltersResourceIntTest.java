package com.rzec.statusvideo.web.rest;

import com.rzec.statusvideo.StatusVideoApp;

import com.rzec.statusvideo.domain.Filters;
import com.rzec.statusvideo.repository.FiltersRepository;
import com.rzec.statusvideo.service.FiltersService;
import com.rzec.statusvideo.service.dto.FiltersDTO;
import com.rzec.statusvideo.service.mapper.FiltersMapper;
import com.rzec.statusvideo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.rzec.statusvideo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FiltersResource REST controller.
 *
 * @see FiltersResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StatusVideoApp.class)
public class FiltersResourceIntTest {

    private static final String DEFAULT_GENRE = "AAAAAAAAAA";
    private static final String UPDATED_GENRE = "BBBBBBBBBB";

    private static final String DEFAULT_TAG = "AAAAAAAAAA";
    private static final String UPDATED_TAG = "BBBBBBBBBB";

    @Autowired
    private FiltersRepository filtersRepository;

    @Autowired
    private FiltersMapper filtersMapper;

    @Autowired
    private FiltersService filtersService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restFiltersMockMvc;

    private Filters filters;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FiltersResource filtersResource = new FiltersResource(filtersService);
        this.restFiltersMockMvc = MockMvcBuilders.standaloneSetup(filtersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Filters createEntity() {
        Filters filters = new Filters()
            .genre(DEFAULT_GENRE)
            .tag(DEFAULT_TAG);
        return filters;
    }

    @Before
    public void initTest() {
        filtersRepository.deleteAll();
        filters = createEntity();
    }

    @Test
    public void createFilters() throws Exception {
        int databaseSizeBeforeCreate = filtersRepository.findAll().size();

        // Create the Filters
        FiltersDTO filtersDTO = filtersMapper.toDto(filters);
        restFiltersMockMvc.perform(post("/api/filters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filtersDTO)))
            .andExpect(status().isCreated());

        // Validate the Filters in the database
        List<Filters> filtersList = filtersRepository.findAll();
        assertThat(filtersList).hasSize(databaseSizeBeforeCreate + 1);
        Filters testFilters = filtersList.get(filtersList.size() - 1);
        assertThat(testFilters.getGenre()).isEqualTo(DEFAULT_GENRE);
        assertThat(testFilters.getTag()).isEqualTo(DEFAULT_TAG);
    }

    @Test
    public void createFiltersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = filtersRepository.findAll().size();

        // Create the Filters with an existing ID
        filters.setId("existing_id");
        FiltersDTO filtersDTO = filtersMapper.toDto(filters);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFiltersMockMvc.perform(post("/api/filters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filtersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Filters in the database
        List<Filters> filtersList = filtersRepository.findAll();
        assertThat(filtersList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllFilters() throws Exception {
        // Initialize the database
        filtersRepository.save(filters);

        // Get all the filtersList
        restFiltersMockMvc.perform(get("/api/filters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(filters.getId())))
            .andExpect(jsonPath("$.[*].genre").value(hasItem(DEFAULT_GENRE.toString())))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG.toString())));
    }

    @Test
    public void getFilters() throws Exception {
        // Initialize the database
        filtersRepository.save(filters);

        // Get the filters
        restFiltersMockMvc.perform(get("/api/filters/{id}", filters.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(filters.getId()))
            .andExpect(jsonPath("$.genre").value(DEFAULT_GENRE.toString()))
            .andExpect(jsonPath("$.tag").value(DEFAULT_TAG.toString()));
    }

    @Test
    public void getNonExistingFilters() throws Exception {
        // Get the filters
        restFiltersMockMvc.perform(get("/api/filters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateFilters() throws Exception {
        // Initialize the database
        filtersRepository.save(filters);
        int databaseSizeBeforeUpdate = filtersRepository.findAll().size();

        // Update the filters
        Filters updatedFilters = filtersRepository.findOne(filters.getId());
        updatedFilters
            .genre(UPDATED_GENRE)
            .tag(UPDATED_TAG);
        FiltersDTO filtersDTO = filtersMapper.toDto(updatedFilters);

        restFiltersMockMvc.perform(put("/api/filters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filtersDTO)))
            .andExpect(status().isOk());

        // Validate the Filters in the database
        List<Filters> filtersList = filtersRepository.findAll();
        assertThat(filtersList).hasSize(databaseSizeBeforeUpdate);
        Filters testFilters = filtersList.get(filtersList.size() - 1);
        assertThat(testFilters.getGenre()).isEqualTo(UPDATED_GENRE);
        assertThat(testFilters.getTag()).isEqualTo(UPDATED_TAG);
    }

    @Test
    public void updateNonExistingFilters() throws Exception {
        int databaseSizeBeforeUpdate = filtersRepository.findAll().size();

        // Create the Filters
        FiltersDTO filtersDTO = filtersMapper.toDto(filters);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFiltersMockMvc.perform(put("/api/filters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filtersDTO)))
            .andExpect(status().isCreated());

        // Validate the Filters in the database
        List<Filters> filtersList = filtersRepository.findAll();
        assertThat(filtersList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteFilters() throws Exception {
        // Initialize the database
        filtersRepository.save(filters);
        int databaseSizeBeforeDelete = filtersRepository.findAll().size();

        // Get the filters
        restFiltersMockMvc.perform(delete("/api/filters/{id}", filters.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Filters> filtersList = filtersRepository.findAll();
        assertThat(filtersList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Filters.class);
        Filters filters1 = new Filters();
        filters1.setId("id1");
        Filters filters2 = new Filters();
        filters2.setId(filters1.getId());
        assertThat(filters1).isEqualTo(filters2);
        filters2.setId("id2");
        assertThat(filters1).isNotEqualTo(filters2);
        filters1.setId(null);
        assertThat(filters1).isNotEqualTo(filters2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FiltersDTO.class);
        FiltersDTO filtersDTO1 = new FiltersDTO();
        filtersDTO1.setId("id1");
        FiltersDTO filtersDTO2 = new FiltersDTO();
        assertThat(filtersDTO1).isNotEqualTo(filtersDTO2);
        filtersDTO2.setId(filtersDTO1.getId());
        assertThat(filtersDTO1).isEqualTo(filtersDTO2);
        filtersDTO2.setId("id2");
        assertThat(filtersDTO1).isNotEqualTo(filtersDTO2);
        filtersDTO1.setId(null);
        assertThat(filtersDTO1).isNotEqualTo(filtersDTO2);
    }
}

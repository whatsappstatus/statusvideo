/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { StatusVideoTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { RzecApplicationPropertiesDetailComponent } from '../../../../../../main/webapp/app/entities/rzec-application-properties/rzec-application-properties-detail.component';
import { RzecApplicationPropertiesService } from '../../../../../../main/webapp/app/entities/rzec-application-properties/rzec-application-properties.service';
import { RzecApplicationProperties } from '../../../../../../main/webapp/app/entities/rzec-application-properties/rzec-application-properties.model';

describe('Component Tests', () => {

    describe('RzecApplicationProperties Management Detail Component', () => {
        let comp: RzecApplicationPropertiesDetailComponent;
        let fixture: ComponentFixture<RzecApplicationPropertiesDetailComponent>;
        let service: RzecApplicationPropertiesService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [StatusVideoTestModule],
                declarations: [RzecApplicationPropertiesDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    RzecApplicationPropertiesService,
                    JhiEventManager
                ]
            }).overrideTemplate(RzecApplicationPropertiesDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RzecApplicationPropertiesDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RzecApplicationPropertiesService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new RzecApplicationProperties('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.rzecApplicationProperties).toEqual(jasmine.objectContaining({id: 'aaa'}));
            });
        });
    });

});

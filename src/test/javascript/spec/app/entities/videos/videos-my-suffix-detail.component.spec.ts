/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { StatusVideoTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { VideosMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/videos/videos-my-suffix-detail.component';
import { VideosMySuffixService } from '../../../../../../main/webapp/app/entities/videos/videos-my-suffix.service';
import { VideosMySuffix } from '../../../../../../main/webapp/app/entities/videos/videos-my-suffix.model';

describe('Component Tests', () => {

    describe('VideosMySuffix Management Detail Component', () => {
        let comp: VideosMySuffixDetailComponent;
        let fixture: ComponentFixture<VideosMySuffixDetailComponent>;
        let service: VideosMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [StatusVideoTestModule],
                declarations: [VideosMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    VideosMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(VideosMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VideosMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VideosMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new VideosMySuffix('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.videos).toEqual(jasmine.objectContaining({id: 'aaa'}));
            });
        });
    });

});

/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { StatusVideoTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { FiltersDetailComponent } from '../../../../../../main/webapp/app/entities/filters/filters-detail.component';
import { FiltersService } from '../../../../../../main/webapp/app/entities/filters/filters.service';
import { Filters } from '../../../../../../main/webapp/app/entities/filters/filters.model';

describe('Component Tests', () => {

    describe('Filters Management Detail Component', () => {
        let comp: FiltersDetailComponent;
        let fixture: ComponentFixture<FiltersDetailComponent>;
        let service: FiltersService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [StatusVideoTestModule],
                declarations: [FiltersDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    FiltersService,
                    JhiEventManager
                ]
            }).overrideTemplate(FiltersDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(FiltersDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FiltersService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Filters('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.filters).toEqual(jasmine.objectContaining({id: 'aaa'}));
            });
        });
    });

});

package com.rzec.statusvideo.repository;

import com.rzec.statusvideo.domain.RzecApplicationProperties;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the RzecApplicationProperties entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RzecApplicationPropertiesRepository extends MongoRepository<RzecApplicationProperties, String> {

    RzecApplicationProperties findByPropertyName(String version);
}

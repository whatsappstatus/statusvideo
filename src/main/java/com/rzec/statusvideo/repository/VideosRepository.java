package com.rzec.statusvideo.repository;

import com.rzec.statusvideo.domain.User;
import com.rzec.statusvideo.domain.Videos;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Videos entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VideosRepository extends MongoRepository<Videos, String> {

    Page<Videos> findAllByActorContainsIgnoreCase(Pageable pageable, String actor);

    Page<Videos> findAllByGenreContainsIgnoreCase(Pageable pageable, String genre);

    Page<Videos> findAllByTagContainsIgnoreCase(Pageable pageable, String tag);

    Page<Videos> findAllByActorContainsIgnoreCaseAndTagContainsIgnoreCase(Pageable pageable, String actor, String tag);

    Page<Videos> findAllByGenreContainsIgnoreCaseAndTagContainsIgnoreCase(Pageable pageable, String genre, String tag);

    Page<Videos> findAllByActorContainsIgnoreCaseAndGenreContainsIgnoreCase(Pageable pageable, String actor, String genre);

    Page<Videos> findAllByActorContainsIgnoreCaseAndGenreContainsIgnoreCaseAndTagContainsIgnoreCase(Pageable pageable, String actor, String genre, String tag);

    Page<Videos> findAllByActorContainsIgnoreCaseOrGenreContainsIgnoreCaseOrTagContainsIgnoreCaseOrNameContainsIgnoreCase(Pageable pageable, String actor, String genre, String tag, String name);

    Page<Videos> findAllByNameContainsIgnoreCase(Pageable pageable, String name);

    Optional<Videos> findOneById(String login);

    //Page<Videos> queryAllBy(TextQuery query, Pageable pageable);

    //@Query(value = "{ $text: { $search: \"vikram vedha\" } }")
    //Page<Videos> queryAllBy(String query, Pageable pageable);
}

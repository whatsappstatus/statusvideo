package com.rzec.statusvideo.repository;

import com.rzec.statusvideo.domain.Filters;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Filters entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FiltersRepository extends MongoRepository<Filters, String> {

    @Query(value="{}", fields="{tag : 1, genre : 1}")
    List<Filters> findTagExcludeGenre();
}

package com.rzec.statusvideo.service.impl;

import com.rzec.statusvideo.service.FiltersService;
import com.rzec.statusvideo.domain.Filters;
import com.rzec.statusvideo.repository.FiltersRepository;
import com.rzec.statusvideo.service.dto.FiltersDTO;
import com.rzec.statusvideo.service.mapper.FiltersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Service Implementation for managing Filters.
 */
@Service
public class FiltersServiceImpl implements FiltersService{

    private final Logger log = LoggerFactory.getLogger(FiltersServiceImpl.class);

    private final FiltersRepository filtersRepository;

    private final FiltersMapper filtersMapper;

    public FiltersServiceImpl(FiltersRepository filtersRepository, FiltersMapper filtersMapper) {
        this.filtersRepository = filtersRepository;
        this.filtersMapper = filtersMapper;
    }

    /**
     * Save a filters.
     *
     * @param filtersDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public FiltersDTO save(FiltersDTO filtersDTO) {
        log.debug("Request to save Filters : {}", filtersDTO);
        Filters filters = filtersMapper.toEntity(filtersDTO);
        filters = filtersRepository.save(filters);
        return filtersMapper.toDto(filters);
    }

    /**
     *  Get all the filters.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<FiltersDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Filters");
        return filtersRepository.findAll(pageable)
            .map(filtersMapper::toDto);
    }

    /**
     *  Get one filters by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public FiltersDTO findOne(String id) {
        log.debug("Request to get Filters : {}", id);
        Filters filters = filtersRepository.findOne(id);
        return filtersMapper.toDto(filters);
    }

    /**
     *  Delete the  filters by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Filters : {}", id);
        filtersRepository.delete(id);
    }

    @Override
    public List<FiltersDTO> findOnlyTags(){
        List<Filters> filters =  filtersRepository.findTagExcludeGenre();
        return filtersMapper.toDto(filters);
    }
}

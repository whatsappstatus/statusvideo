package com.rzec.statusvideo.service.impl;

import com.rzec.statusvideo.config.Constants;
import com.rzec.statusvideo.service.VideosService;
import com.rzec.statusvideo.domain.Videos;
import com.rzec.statusvideo.repository.VideosRepository;
import com.rzec.statusvideo.service.dto.VideosDTO;
import com.rzec.statusvideo.service.mapper.VideosMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing Videos.
 */
@Service
public class VideosServiceImpl implements VideosService{

    private final Logger log = LoggerFactory.getLogger(VideosServiceImpl.class);

    private final VideosRepository videosRepository;

    private final VideosMapper videosMapper;

    public VideosServiceImpl(VideosRepository videosRepository, VideosMapper videosMapper) {
        this.videosRepository = videosRepository;
        this.videosMapper = videosMapper;
    }

    /**
     * Save a videos.
     *
     * @param videosDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public VideosDTO save(VideosDTO videosDTO) {
        log.debug("Request to save Videos : {}", videosDTO);
        Videos videos = videosMapper.toEntity(videosDTO);
        videos = videosRepository.save(videos);
        return videosMapper.toDto(videos);
    }

    /**
     *  Get all the videos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<VideosDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Videos");
        return videosRepository.findAll(pageable)
            .map(videosMapper::toDto);
    }

    /**
     *  Get one videos by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public VideosDTO findOne(String id) {
        log.debug("Request to get Videos : {}", id);
        Videos videos = videosRepository.findOne(id);
        return videosMapper.toDto(videos);
    }

    /**
     *  Delete the  videos by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Videos : {}", id);
        videosRepository.delete(id);
    }

    /**
     *  Get all the  videos by actor.
     *
     *  @param actor the id of the entity
     */
    @Override
    public Page<VideosDTO> getAllVideosByActor(Pageable pageable, String actor) {
        log.debug("Request to get Videos by actor : {}", actor);
        return videosRepository.findAllByActorContainsIgnoreCase(pageable, actor).map(videosMapper::toDto);
    }

    /**
     *  Get all the  videos by Genre.
     *
     *  @param genre the id of the entity
     */
    @Override
    public Page<VideosDTO> getAllVideosByGenre(Pageable pageable, String genre){
        log.debug("Request to get Videos by Genre : {}", genre);
        return videosRepository.findAllByGenreContainsIgnoreCase(pageable, genre).map(videosMapper::toDto);
    }

    /**
     *  Get all the  videos by Tag.
     *
     *  @param tag the id of the entity
     */
    @Override
    public Page<VideosDTO> getAllVideosByTag(Pageable pageable, String tag){
        log.debug("Request to get Videos by Tag : {}", tag);
        return videosRepository.findAllByTagContainsIgnoreCase(pageable, tag).map(videosMapper::toDto);
    }

    /**
     *  Get all the  videos by Actor and Genre.
     *
     *  @param actor,genre the id of the entity
     */
    @Override
   public Page<VideosDTO> getAllByActorAndGenre(Pageable pageable, String actor, String genre){
       log.debug("Request to get Videos by actor and Genre: {}", actor, genre);
       return videosRepository.findAllByActorContainsIgnoreCaseAndGenreContainsIgnoreCase(pageable, actor, genre).map(videosMapper::toDto);
   }

   @Override
    public Page<VideosDTO> getAllByActorAndTag(Pageable pageable, String actor, String tag){
        log.debug("Request to get Videos by actor and Tag: {}", actor, tag);
        return videosRepository.findAllByActorContainsIgnoreCaseAndTagContainsIgnoreCase(pageable, actor, tag).map(videosMapper::toDto);
    }

    @Override
    public Page<VideosDTO> getAllByGenreAndTag(Pageable pageable, String genre, String tag){
        log.debug("Request to get Videos by Genre and Tag: {}", genre,tag);
        return videosRepository.findAllByGenreContainsIgnoreCaseAndTagContainsIgnoreCase(pageable, genre, tag).map(videosMapper::toDto);
    }

    @Override
    public Page<VideosDTO> getAllByActorAndGenreAndTag(Pageable pageable, String actor, String genre, String tag){
        log.debug("Request to get Videos by actor and Genre and Tag: {}", actor, genre, tag);
        return videosRepository.findAllByActorContainsIgnoreCaseAndGenreContainsIgnoreCaseAndTagContainsIgnoreCase(pageable, actor, genre, tag).map(videosMapper::toDto);
    }

    @Override
    public Page<VideosDTO> getAllVideosByName(Pageable pageable, String name){
        log.debug("Request to get Videos by Movie Name : {}", name);
        return videosRepository.findAllByNameContainsIgnoreCase(pageable, name).map(videosMapper::toDto);
    }

    @Override
    public Page<VideosDTO> getAllVideosByGeneralSearch(Pageable pageable, String general_query1,String general_query2,String general_query3,String general_query4){
        log.debug("Request to get Videos by General Search : {}", general_query1);


        //TextQuery query = TextQuery.queryText(TextCriteria.forDefaultLanguage().caseSensitive(false).matchingPhrase("vikram vedha"));
        return videosRepository.findAllByActorContainsIgnoreCaseOrGenreContainsIgnoreCaseOrTagContainsIgnoreCaseOrNameContainsIgnoreCase(pageable,general_query1,general_query2,general_query3, general_query4)
            .map(videosMapper::toDto);
    }

    @Override
    public boolean updateVideos(String id, int views){
        try{
        videosRepository.findOneById(id).ifPresent(videos -> {
           videos.setViews(views);
           videosRepository.save(videos);
       });
        return true;
    }catch (Exception e){
            return  false;
        }
    }
}

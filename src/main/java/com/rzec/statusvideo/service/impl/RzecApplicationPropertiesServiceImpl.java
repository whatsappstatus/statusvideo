package com.rzec.statusvideo.service.impl;

import com.rzec.statusvideo.service.RzecApplicationPropertiesService;
import com.rzec.statusvideo.domain.RzecApplicationProperties;
import com.rzec.statusvideo.repository.RzecApplicationPropertiesRepository;
import com.rzec.statusvideo.service.dto.RzecApplicationPropertiesDTO;
import com.rzec.statusvideo.service.mapper.RzecApplicationPropertiesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing RzecApplicationProperties.
 */
@Service
public class RzecApplicationPropertiesServiceImpl implements RzecApplicationPropertiesService{

    private final Logger log = LoggerFactory.getLogger(RzecApplicationPropertiesServiceImpl.class);

    private final RzecApplicationPropertiesRepository rzecApplicationPropertiesRepository;

    private final RzecApplicationPropertiesMapper rzecApplicationPropertiesMapper;

    public RzecApplicationPropertiesServiceImpl(RzecApplicationPropertiesRepository rzecApplicationPropertiesRepository, RzecApplicationPropertiesMapper rzecApplicationPropertiesMapper) {
        this.rzecApplicationPropertiesRepository = rzecApplicationPropertiesRepository;
        this.rzecApplicationPropertiesMapper = rzecApplicationPropertiesMapper;
    }

    /**
     * Save a rzecApplicationProperties.
     *
     * @param rzecApplicationPropertiesDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RzecApplicationPropertiesDTO save(RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO) {
        log.debug("Request to save RzecApplicationProperties : {}", rzecApplicationPropertiesDTO);
        RzecApplicationProperties rzecApplicationProperties = rzecApplicationPropertiesMapper.toEntity(rzecApplicationPropertiesDTO);
        rzecApplicationProperties = rzecApplicationPropertiesRepository.save(rzecApplicationProperties);
        return rzecApplicationPropertiesMapper.toDto(rzecApplicationProperties);
    }

    /**
     *  Get all the rzecApplicationProperties.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<RzecApplicationPropertiesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RzecApplicationProperties");
        return rzecApplicationPropertiesRepository.findAll(pageable)
            .map(rzecApplicationPropertiesMapper::toDto);
    }

    /**
     *  Get one rzecApplicationProperties by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public RzecApplicationPropertiesDTO findOne(String id) {
        log.debug("Request to get RzecApplicationProperties : {}", id);
        RzecApplicationProperties rzecApplicationProperties = rzecApplicationPropertiesRepository.findOne(id);
        return rzecApplicationPropertiesMapper.toDto(rzecApplicationProperties);
    }

    /**
     *  Delete the  rzecApplicationProperties by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete RzecApplicationProperties : {}", id);
        rzecApplicationPropertiesRepository.delete(id);
    }

    public RzecApplicationPropertiesDTO getApkVersion(String apk_version){
        log.debug("Getting APK-Version: {}" +apk_version);
        return rzecApplicationPropertiesMapper.toDto(rzecApplicationPropertiesRepository.findByPropertyName(apk_version));
    }
}

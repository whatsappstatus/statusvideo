package com.rzec.statusvideo.service;

import com.rzec.statusvideo.service.dto.FiltersDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Filters.
 */
public interface FiltersService {

    /**
     * Save a filters.
     *
     * @param filtersDTO the entity to save
     * @return the persisted entity
     */
    FiltersDTO save(FiltersDTO filtersDTO);

    /**
     *  Get all the filters.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<FiltersDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" filters.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    FiltersDTO findOne(String id);

    /**
     *  Delete the "id" filters.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    List<FiltersDTO> findOnlyTags();
}

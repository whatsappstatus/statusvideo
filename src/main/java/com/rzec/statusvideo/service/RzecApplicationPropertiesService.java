package com.rzec.statusvideo.service;

import com.rzec.statusvideo.service.dto.RzecApplicationPropertiesDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing RzecApplicationProperties.
 */
public interface RzecApplicationPropertiesService {

    /**
     * Save a rzecApplicationProperties.
     *
     * @param rzecApplicationPropertiesDTO the entity to save
     * @return the persisted entity
     */
    RzecApplicationPropertiesDTO save(RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO);

    /**
     *  Get all the rzecApplicationProperties.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<RzecApplicationPropertiesDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" rzecApplicationProperties.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RzecApplicationPropertiesDTO findOne(String id);

    /**
     *  Delete the "id" rzecApplicationProperties.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    /**
     *  Find the "apk_version" rzecApplicationProperties.
     *
     *  @param apk_version
     */

    public RzecApplicationPropertiesDTO getApkVersion(String apk_version);
}

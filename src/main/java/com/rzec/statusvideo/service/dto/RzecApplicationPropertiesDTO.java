package com.rzec.statusvideo.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RzecApplicationProperties entity.
 */
public class RzecApplicationPropertiesDTO implements Serializable {

    private String id;

    private String propertyName;

    private String description;

    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO = (RzecApplicationPropertiesDTO) o;
        if(rzecApplicationPropertiesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rzecApplicationPropertiesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RzecApplicationPropertiesDTO{" +
            "id=" + getId() +
            ", propertyName='" + getPropertyName() + "'" +
            ", description='" + getDescription() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }
}

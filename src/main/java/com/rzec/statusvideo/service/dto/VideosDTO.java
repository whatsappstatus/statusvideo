package com.rzec.statusvideo.service.dto;


import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import com.rzec.statusvideo.domain.enumeration.Language;

/**
 * A DTO for the Videos entity.
 */
public class VideosDTO implements Serializable {

    private String id;

    private String name;

    private String url;

    private Integer views;

    private String tag;

    private String actor;

    private ZonedDateTime createdDate;

    private String genre;

    private Language language;

    private String credits;

    private String image_url;

    private String movie_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getCredits() {
        return credits;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getMovie_name() {
        return movie_name;
    }

    public void setMovie_name(String movie_name) {
        this.movie_name = movie_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideosDTO videosDTO = (VideosDTO) o;
        if(videosDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), videosDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "VideosDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", views='" + getViews() + "'" +
            ", tag='" + getTag() + "'" +
            ", actor='" + getActor() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", genre='" + getGenre() + "'" +
            ", language='" + getLanguage() + "'" +
            ", credits='" + getCredits() + "'" +
            ", image_url='" + getImage_url() + "'" +
            ", movie_name='" + getMovie_name() + "'" +
            "}";
    }
}

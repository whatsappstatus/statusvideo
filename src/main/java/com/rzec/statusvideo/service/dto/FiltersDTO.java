package com.rzec.statusvideo.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Filters entity.
 */
public class FiltersDTO implements Serializable {

    private String id;

    private String genre;

    private String tag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FiltersDTO filtersDTO = (FiltersDTO) o;
        if(filtersDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), filtersDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FiltersDTO{" +
            "id=" + getId() +
            ", genre='" + getGenre() + "'" +
            ", tag='" + getTag() + "'" +
            "}";
    }
}

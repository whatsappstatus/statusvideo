package com.rzec.statusvideo.service;

import com.rzec.statusvideo.service.dto.VideosDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Videos.
 */
public interface VideosService {

    /**
     * Save a videos.
     *
     * @param videosDTO the entity to save
     * @return the persisted entity
     */
    VideosDTO save(VideosDTO videosDTO);

    /**
     *  Get all the videos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<VideosDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" videos.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    VideosDTO findOne(String id);

    /**
     *  Delete the "id" videos.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    public Page<VideosDTO> getAllVideosByActor(Pageable pageable, String actor);

    public Page<VideosDTO> getAllVideosByGenre(Pageable pageable, String genre);

    public Page<VideosDTO> getAllVideosByTag(Pageable pageable, String tag);

    public Page<VideosDTO> getAllByActorAndGenre(Pageable pageable, String actor, String genre);

    public Page<VideosDTO> getAllByActorAndTag(Pageable pageable, String actor, String tag);

    public Page<VideosDTO> getAllByGenreAndTag(Pageable pageable, String genre, String tag);

    public Page<VideosDTO> getAllByActorAndGenreAndTag(Pageable pageable, String actor, String genre, String tag);

    public Page<VideosDTO> getAllVideosByName(Pageable pageable, String name);

    public Page<VideosDTO> getAllVideosByGeneralSearch(Pageable pageable, String general_query1,String general_query2,String general_query3, String general_query4);

    public boolean updateVideos(String id, int views);
}

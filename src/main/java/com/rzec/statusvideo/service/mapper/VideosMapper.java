package com.rzec.statusvideo.service.mapper;

import com.rzec.statusvideo.domain.*;
import com.rzec.statusvideo.service.dto.VideosDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Videos and its DTO VideosDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VideosMapper extends EntityMapper<VideosDTO, Videos> {

    

    
}

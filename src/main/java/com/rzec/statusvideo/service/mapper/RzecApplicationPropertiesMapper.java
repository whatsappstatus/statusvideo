package com.rzec.statusvideo.service.mapper;

import com.rzec.statusvideo.domain.*;
import com.rzec.statusvideo.service.dto.RzecApplicationPropertiesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RzecApplicationProperties and its DTO RzecApplicationPropertiesDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RzecApplicationPropertiesMapper extends EntityMapper<RzecApplicationPropertiesDTO, RzecApplicationProperties> {

    

    
}

package com.rzec.statusvideo.service.mapper;

import com.rzec.statusvideo.domain.*;
import com.rzec.statusvideo.service.dto.FiltersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Filters and its DTO FiltersDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FiltersMapper extends EntityMapper<FiltersDTO, Filters> {

    

    
}

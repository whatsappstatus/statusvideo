package com.rzec.statusvideo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rzec.statusvideo.service.FiltersService;
import com.rzec.statusvideo.web.rest.errors.BadRequestAlertException;
import com.rzec.statusvideo.web.rest.util.HeaderUtil;
import com.rzec.statusvideo.web.rest.util.PaginationUtil;
import com.rzec.statusvideo.service.dto.FiltersDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Filters.
 */
@RestController
@RequestMapping("/api")
public class FiltersResource {

    private final Logger log = LoggerFactory.getLogger(FiltersResource.class);

    private static final String ENTITY_NAME = "filters";

    private final FiltersService filtersService;

    public FiltersResource(FiltersService filtersService) {
        this.filtersService = filtersService;
    }

    /**
     * POST  /filters : Create a new filters.
     *
     * @param filtersDTO the filtersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new filtersDTO, or with status 400 (Bad Request) if the filters has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/filters")
    @Timed
    public ResponseEntity<FiltersDTO> createFilters(@RequestBody FiltersDTO filtersDTO) throws URISyntaxException {
        log.debug("REST request to save Filters : {}", filtersDTO);
        if (filtersDTO.getId() != null) {
            throw new BadRequestAlertException("A new filters cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FiltersDTO result = filtersService.save(filtersDTO);
        return ResponseEntity.created(new URI("/api/filters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /filters : Updates an existing filters.
     *
     * @param filtersDTO the filtersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated filtersDTO,
     * or with status 400 (Bad Request) if the filtersDTO is not valid,
     * or with status 500 (Internal Server Error) if the filtersDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/filters")
    @Timed
    public ResponseEntity<FiltersDTO> updateFilters(@RequestBody FiltersDTO filtersDTO) throws URISyntaxException {
        log.debug("REST request to update Filters : {}", filtersDTO);
        if (filtersDTO.getId() == null) {
            return createFilters(filtersDTO);
        }
        FiltersDTO result = filtersService.save(filtersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, filtersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /filters : get all the filters.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of filters in body
     */
    @GetMapping("/filters")
    @Timed
    public ResponseEntity<List<FiltersDTO>> getAllFilters(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Filters");
        Page<FiltersDTO> page = filtersService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/filters");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /filters : get all the filters.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of filters in body
     */
    /**
     * GET  /filters : get all the filters.
     *
     * @param filter the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of filters in body
     */
    @GetMapping("/filter")
    @Timed
    public ResponseEntity<String> getAllTags(@ApiParam String filter) {
        log.debug("REST request to get a page of Filters");
        List<FiltersDTO> page = filtersService.findOnlyTags();

        JSONObject tag = new JSONObject();
        JSONArray tagsArray = new JSONArray();
        JSONObject genre = new JSONObject();
        JSONArray genreArray = new JSONArray();

        for(FiltersDTO f : page){
            tagsArray.put(f.getTag());
            genreArray.put(f.getGenre());
        }

        try {
            tag.put("tags",tagsArray);
            genre.put("genre",genreArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/filters");
        if(filter.equalsIgnoreCase("tag")){
            return new ResponseEntity<>(tag.toString(), HttpStatus.OK);
        }
        else if(filter.equalsIgnoreCase("genre")){
            return new ResponseEntity<>(genre.toString(), HttpStatus.OK);
        }
        return new ResponseEntity<>("Please provide which filter you want as Query parameter.. eg.. /filter?filter=tag", HttpStatus.OK);
    }
    /**
     * GET  /filters/:id : get the "id" filters.
     *
     * @param id the id of the filtersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the filtersDTO, or with status 404 (Not Found)
     */
    @GetMapping("/filters/{id}")
    @Timed
    public ResponseEntity<FiltersDTO> getFilters(@PathVariable String id) {
        log.debug("REST request to get Filters : {}", id);
        FiltersDTO filtersDTO = filtersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(filtersDTO));
    }

    /**
     * DELETE  /filters/:id : delete the "id" filters.
     *
     * @param id the id of the filtersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/filters/{id}")
    @Timed
    public ResponseEntity<Void> deleteFilters(@PathVariable String id) {
        log.debug("REST request to delete Filters : {}", id);
        filtersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}

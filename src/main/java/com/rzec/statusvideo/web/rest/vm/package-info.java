/**
 * View Models used by Spring MVC REST controllers.
 */
package com.rzec.statusvideo.web.rest.vm;

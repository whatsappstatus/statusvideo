package com.rzec.statusvideo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rzec.statusvideo.domain.enumeration.Language;
import com.rzec.statusvideo.service.VideosService;
import com.rzec.statusvideo.web.rest.errors.BadRequestAlertException;
import com.rzec.statusvideo.web.rest.util.HeaderUtil;
import com.rzec.statusvideo.web.rest.util.PaginationUtil;
import com.rzec.statusvideo.service.dto.VideosDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.json.simple.*;

import javax.imageio.ImageIO;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Predicates.equalTo;

/**
 * REST controller for managing Videos.
 */
@RestController
@RequestMapping("/api")
public class VideosResource {

    private final Logger log = LoggerFactory.getLogger(VideosResource.class);

    private static final String ENTITY_NAME = "videos";

    private static final String SEARCH_BY_ACTOR = "A";
    private static final String SEARCH_BY_GENRE = "G";
    private static final String SEARCH_BY_TAG = "T";
    private static final String SEARCH_BY_ACTOR_AND_GENRE = "AG";
    private static final String SEARCH_BY_ACTOR_AND_TAG = "AT";
    private static final String SEARCH_BY_GENRE_AND_TAG = "GT";
    private static final String SEARCH_BY_ACTOR_AND_GENRE_AND_TAG = "AGT";
    private static final String SEARCH_BY_MOVIE_NAME = "MN";
    private static final String SEARCH_BY_COMMON = "CO";

    private final VideosService videosService;

    public VideosResource(VideosService videosService) {
        this.videosService = videosService;
    }

    /**
     * POST  /videos : Create a new videos.
     *
     * @param videosDTO the videosDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new videosDTO, or with status 400 (Bad Request) if the videos has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/videos")
    @Timed
    public ResponseEntity<VideosDTO> createVideos(@RequestBody VideosDTO videosDTO) throws URISyntaxException {
        log.debug("REST request to save Videos : {}", videosDTO);
        ZonedDateTime z = ZonedDateTime.now(ZoneId.systemDefault());
        videosDTO.setCreatedDate(z);
        if (videosDTO.getId() != null) {
            throw new BadRequestAlertException("A new videos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VideosDTO result = videosService.save(videosDTO);
        return ResponseEntity.created(new URI("/api/videos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * PUT  /videos : Updates an existing videos.
     *
     * @param videosDTO the videosDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated videosDTO,
     * or with status 400 (Bad Request) if the videosDTO is not valid,
     * or with status 500 (Internal Server Error) if the videosDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/videos")
    @Timed
    public ResponseEntity<VideosDTO> updateVideos(@RequestBody VideosDTO videosDTO) throws URISyntaxException {
        log.debug("REST request to update Videos : {}", videosDTO);
        if (videosDTO.getId() == null) {
            return createVideos(videosDTO);
        }
        VideosDTO result = videosService.save(videosDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, videosDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /videos : get all the videos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of videos in body
     */
    @GetMapping("/videos")
    @Timed
    public ResponseEntity<List<VideosDTO>> getAllVideos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Videos"+pageable.getSort().toString());
        Page<VideosDTO> page = videosService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/videos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /videos : get all the videos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of videos in body
     */
    @GetMapping("/getVideos")
    @Timed
    public ResponseEntity<Page<VideosDTO>> getAllVideosForAngular(@ApiParam Pageable pageable) {
        log.debug("REST request to get a list of Videos");
        Page<VideosDTO> page = videosService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/getVideos");
        return new ResponseEntity<>(page, headers, HttpStatus.OK);
    }
    /**
     * GET  /videos/:id : get the "id" videos.
     *
     * @param id the id of the videosDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the videosDTO, or with status 404 (Not Found)
     */
    @GetMapping("/videos/{id}")
    @Timed
    public ResponseEntity<VideosDTO> getVideos(@PathVariable String id) {
        log.debug("REST request to get Videos : {}", id);
        VideosDTO videosDTO = videosService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(videosDTO));
    }

    /**
     * GET  /videos/:id : get the "id" videos.
     *
     * @param id the id of the videosDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the videosDTO, or with status 404 (Not Found)
     */
    @GetMapping("/updateViews/{id}")
    @Timed
    public ResponseEntity<Boolean> updateViews(@PathVariable String id) {
        log.debug("REST request to Update Views of the Video : {}", id);
        VideosDTO videosDTO = videosService.findOne(id);
        int views = videosDTO.getViews() + 1;
        boolean isViewUpdated = videosService.updateVideos(id, views);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(isViewUpdated));
    }

    /**
     * DELETE  /videos/:id : delete the "id" videos.
     *
     * @param id the id of the videosDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/videos/{id}")
    @Timed
    public ResponseEntity<Void> deleteVideos(@PathVariable String id) {
        log.debug("REST request to delete Videos : {}", id);
        videosService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * GET  /videos : get all the videos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of videos in body
     */
    @PostMapping("/searchVideos")
    @Timed
    public ResponseEntity<Page<VideosDTO>> getAllVideosByActor(@RequestBody String json, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Videos by actor");

        Page<VideosDTO> page = null;
        String actor = null;
        String genre = null;
        String searchBy = null;
        String tag = null;
        String movie_name = null;
        String general_query1 = null;
        String general_query2 = null;
        String general_query3 = null;
        String general_query4 = null;



        try {
            JSONObject filter=new JSONObject(json);

            searchBy = filter.getString("searchBy");
            actor = filter.getString("actor");
            genre = filter.getString("genre");
            tag = filter.getString("tag");
            movie_name = filter.getString("movie_name");
            general_query1 = filter.getString("general_query");
            general_query2 = general_query3 = general_query4 = general_query1;

            if(searchBy.equalsIgnoreCase(SEARCH_BY_ACTOR)){
                if(actor != null){
                    page = videosService.getAllVideosByActor(pageable,actor);
                }
            }else if(searchBy.equalsIgnoreCase(SEARCH_BY_GENRE)){
                if(genre != null){
                    page = videosService.getAllVideosByGenre(pageable, genre);
                }
            }else if(searchBy.equalsIgnoreCase(SEARCH_BY_TAG)){
                if(tag != null){
                    page = videosService.getAllVideosByTag(pageable, tag);
                }
            }else if(searchBy.equalsIgnoreCase(SEARCH_BY_ACTOR_AND_GENRE)) {
                if(actor != null && genre != null){
                    page = videosService.getAllByActorAndGenre(pageable, actor, genre);
                }
            }else if(searchBy.equalsIgnoreCase(SEARCH_BY_ACTOR_AND_TAG)) {
                if(actor != null && tag != null){
                    page = videosService.getAllByActorAndTag(pageable, actor, tag);
                }
            }else if(searchBy.equalsIgnoreCase(SEARCH_BY_GENRE_AND_TAG)) {
                if(tag != null && genre != null){
                    page = videosService.getAllByGenreAndTag(pageable, genre, tag);
                }
            }else if(searchBy.equalsIgnoreCase(SEARCH_BY_ACTOR_AND_GENRE_AND_TAG)) {
                if(actor != null && genre != null && tag != null){
                    page = videosService.getAllByActorAndGenreAndTag(pageable, actor, genre, tag);
                }
            }else if(searchBy.equalsIgnoreCase(SEARCH_BY_COMMON)) {
                if(general_query1 != null){
                    page = videosService.getAllVideosByGeneralSearch(pageable, general_query1,general_query2,general_query3,general_query4);
                }
            }else if(searchBy.equalsIgnoreCase(SEARCH_BY_MOVIE_NAME)) {
                if(movie_name != null){
                    page = videosService.getAllVideosByName(pageable, movie_name);
                }
            }
        } catch (JSONException e) {
            log.error("Exception while searching the videos",e);
            e.printStackTrace();
        }


        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/searchVideos");
        return new ResponseEntity<>(page, headers, HttpStatus.OK);
    }


    @PostMapping("/storeVideos")
    @Timed
    public ResponseEntity<Boolean> storeVideos(@RequestPart("file") MultipartFile file) {
        log.debug("REST request to store all the videos from JSON file");

        boolean result = false;
        JSONParser parser = new JSONParser();
        VideosDTO videosDTO = new VideosDTO();
        try{


            String content = new String(file.getBytes());
            Object obj = parser.parse(content);
            JSONArray jarray = new JSONArray();
            org.json.simple.JSONObject val = new org.json.simple.JSONObject();
            for (Object o : jarray = (JSONArray) obj) {
                 val = (org.json.simple.JSONObject) o;

                ZonedDateTime z = ZonedDateTime.now(ZoneId.systemDefault());
                videosDTO.setCreatedDate(z);


                 videosDTO.setActor(val.get("actor").toString());
                 videosDTO.setGenre(val.get("genre").toString());
                 videosDTO.setImage_url(val.get("image_url").toString());
                 videosDTO.setCredits(val.get("credits").toString());
                 videosDTO.setLanguage(Language.TAMIL);
                 videosDTO.setUrl(val.get("url").toString());
                 videosDTO.setViews(1);
                 videosDTO.setName(val.get("name").toString());
                 videosDTO.setTag(val.get("tag").toString());

                VideosDTO resultDto = videosService.save(videosDTO);
            }

            result = true;

        }catch (Exception e){
            result = false;
            log.error("There is an issue while parsing the JSON file",e);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/getVideoScreen")
    @Timed
    public ResponseEntity<Boolean> getScreenshot(@RequestPart("file") MultipartFile file) {
        log.debug("REST request to store all the videos from JSON file");
        boolean result = true;


            System.setProperty("java.awt.headless", "false");

        try {
            Robot robot = new Robot();
            String format = "jpg";
            String fileName = "FullScreenshot." + format;

            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
            File f = new File("C:\\Users\\Karthik\\Desktop\\"+fileName);
            ImageIO.write(screenFullImage, format, f);

            System.out.println("A full screenshot saved!");
            } catch (AWTException | IOException ex) {
            System.err.println(ex);
            }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}

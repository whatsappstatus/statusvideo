package com.rzec.statusvideo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rzec.statusvideo.service.RzecApplicationPropertiesService;
import com.rzec.statusvideo.web.rest.errors.BadRequestAlertException;
import com.rzec.statusvideo.web.rest.util.HeaderUtil;
import com.rzec.statusvideo.web.rest.util.PaginationUtil;
import com.rzec.statusvideo.service.dto.RzecApplicationPropertiesDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RzecApplicationProperties.
 */
@RestController
@RequestMapping("/api")
public class RzecApplicationPropertiesResource {

    private final Logger log = LoggerFactory.getLogger(RzecApplicationPropertiesResource.class);

    private static final String ENTITY_NAME = "rzecApplicationProperties";

    private final RzecApplicationPropertiesService rzecApplicationPropertiesService;

    public RzecApplicationPropertiesResource(RzecApplicationPropertiesService rzecApplicationPropertiesService) {
        this.rzecApplicationPropertiesService = rzecApplicationPropertiesService;
    }

    /**
     * POST  /rzec-application-properties : Create a new rzecApplicationProperties.
     *
     * @param rzecApplicationPropertiesDTO the rzecApplicationPropertiesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rzecApplicationPropertiesDTO, or with status 400 (Bad Request) if the rzecApplicationProperties has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rzec-application-properties")
    @Timed
    public ResponseEntity<RzecApplicationPropertiesDTO> createRzecApplicationProperties(@RequestBody RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO) throws URISyntaxException {
        log.debug("REST request to save RzecApplicationProperties : {}", rzecApplicationPropertiesDTO);
        if (rzecApplicationPropertiesDTO.getId() != null) {
            throw new BadRequestAlertException("A new rzecApplicationProperties cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RzecApplicationPropertiesDTO result = rzecApplicationPropertiesService.save(rzecApplicationPropertiesDTO);
        return ResponseEntity.created(new URI("/api/rzec-application-properties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rzec-application-properties : Updates an existing rzecApplicationProperties.
     *
     * @param rzecApplicationPropertiesDTO the rzecApplicationPropertiesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rzecApplicationPropertiesDTO,
     * or with status 400 (Bad Request) if the rzecApplicationPropertiesDTO is not valid,
     * or with status 500 (Internal Server Error) if the rzecApplicationPropertiesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rzec-application-properties")
    @Timed
    public ResponseEntity<RzecApplicationPropertiesDTO> updateRzecApplicationProperties(@RequestBody RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO) throws URISyntaxException {
        log.debug("REST request to update RzecApplicationProperties : {}", rzecApplicationPropertiesDTO);
        if (rzecApplicationPropertiesDTO.getId() == null) {
            return createRzecApplicationProperties(rzecApplicationPropertiesDTO);
        }
        RzecApplicationPropertiesDTO result = rzecApplicationPropertiesService.save(rzecApplicationPropertiesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rzecApplicationPropertiesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rzec-application-properties : get all the rzecApplicationProperties.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of rzecApplicationProperties in body
     */
    @GetMapping("/rzec-application-properties")
    @Timed
    public ResponseEntity<List<RzecApplicationPropertiesDTO>> getAllRzecApplicationProperties(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RzecApplicationProperties");
        Page<RzecApplicationPropertiesDTO> page = rzecApplicationPropertiesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rzec-application-properties");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rzec-application-properties : get all the rzecApplicationProperties.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of rzecApplicationProperties in body
     */
    @GetMapping("/app_filters")
    @Timed
    public ResponseEntity<String> getRzecAppProperties(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RzecApplicationProperties");
        Page<RzecApplicationPropertiesDTO> page = rzecApplicationPropertiesService.findAll(pageable);

        JSONObject props = new JSONObject();

        try {
            for(int i=0; i < page.getContent().size(); i++){
                List<String> result = Arrays.asList(page.getContent().get(i).getValue().split("\\s*,\\s*"));
                props.put(page.getContent().get(i).getPropertyName(),result);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rzec-application-properties");
        return new ResponseEntity<>(props.toString(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rzec-application-properties/:id : get the "id" rzecApplicationProperties.
     *
     * @param id the id of the rzecApplicationPropertiesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rzecApplicationPropertiesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rzec-application-properties/{id}")
    @Timed
    public ResponseEntity<RzecApplicationPropertiesDTO> getRzecApplicationProperties(@PathVariable String id) {
        log.debug("REST request to get RzecApplicationProperties : {}", id);
        RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO = rzecApplicationPropertiesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rzecApplicationPropertiesDTO));
    }

    /**
     * DELETE  /rzec-application-properties/:id : delete the "id" rzecApplicationProperties.
     *
     * @param id the id of the rzecApplicationPropertiesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rzec-application-properties/{id}")
    @Timed
    public ResponseEntity<Void> deleteRzecApplicationProperties(@PathVariable String id) {
        log.debug("REST request to delete RzecApplicationProperties : {}", id);
        rzecApplicationPropertiesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * GET  /rzec-application-properties/:id : get the "id" rzecApplicationProperties.
     *
     *
     * @return the ResponseEntity with status 200 (OK) and with body the rzecApplicationPropertiesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/apk_version")
    @Timed
    public ResponseEntity<String> getApkVersion() {
        log.debug("REST request to get apk version : {}");
        RzecApplicationPropertiesDTO rzecApplicationPropertiesDTO = rzecApplicationPropertiesService.getApkVersion("apk_version");
        JSONObject apk_result = new JSONObject();
        try {
            apk_result.put("apk_version",rzecApplicationPropertiesDTO.getValue());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(apk_result.toString()));
    }
}

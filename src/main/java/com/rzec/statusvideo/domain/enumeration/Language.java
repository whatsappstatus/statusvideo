package com.rzec.statusvideo.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    TAMIL, HINDI, TELUGU
}

package com.rzec.statusvideo.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.rzec.statusvideo.domain.enumeration.Language;

/**
 * A Videos.
 */
@Document(collection = "videos")
public class Videos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("url")
    private String url;

    @Field("views")
    private Integer views;

    @Field("tag")
    private String tag;

    @Field("actor")
    private String actor;

    @Field("created_date")
    private ZonedDateTime createdDate;

    @Field("genre")
    private String genre;

    @Field("language")
    private Language language;

    @Field("credits")
    private String credits;

    @Field("image_url")
    private String image_url;

    @Field("movie_name")
    private String movie_name;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Videos name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public Videos url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getViews() {
        return views;
    }

    public Videos views(Integer views) {
        this.views = views;
        return this;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public String getTag() {
        return tag;
    }

    public Videos tag(String tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getActor() {
        return actor;
    }

    public Videos actor(String actor) {
        this.actor = actor;
        return this;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Videos createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getGenre() {
        return genre;
    }

    public Videos genre(String genre) {
        this.genre = genre;
        return this;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Language getLanguage() {
        return language;
    }

    public Videos language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getCredits() {
        return credits;
    }

    public Videos credits(String credits) {
        this.credits = credits;
        return this;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }

    public String getImage_url() {
        return image_url;
    }

    public Videos image_url(String image_url) {
        this.image_url = image_url;
        return this;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getMovie_name() {
        return movie_name;
    }

    public Videos movie_name(String movie_name) {
        this.movie_name = movie_name;
        return this;
    }

    public void setMovie_name(String movie_name) {
        this.movie_name = movie_name;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Videos videos = (Videos) o;
        if (videos.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), videos.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Videos{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", views='" + getViews() + "'" +
            ", tag='" + getTag() + "'" +
            ", actor='" + getActor() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", genre='" + getGenre() + "'" +
            ", language='" + getLanguage() + "'" +
            ", credits='" + getCredits() + "'" +
            ", image_url='" + getImage_url() + "'" +
            ", movie_name='" + getMovie_name() + "'" +
            "}";
    }
}

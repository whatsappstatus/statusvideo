package com.rzec.statusvideo.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Filters.
 */
@Document(collection = "filters")
public class Filters implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    @Field("genre")
    private String genre;

    @Field("tag")
    private String tag;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public Filters genre(String genre) {
        this.genre = genre;
        return this;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTag() {
        return tag;
    }

    public Filters tag(String tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Filters filters = (Filters) o;
        if (filters.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), filters.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Filters{" +
            "id=" + getId() +
            ", genre='" + getGenre() + "'" +
            ", tag='" + getTag() + "'" +
            "}";
    }
}

package com.rzec.statusvideo.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.io.Serializable;
import java.util.Objects;

/**
 * A RzecApplicationProperties.
 */
@Document(collection = "rzec_application_properties")
public class RzecApplicationProperties implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    @Field("property_name")
    private String propertyName;

    @Field("description")
    private String description;

    @Field("value")
    private String value;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public RzecApplicationProperties propertyName(String propertyName) {
        this.propertyName = propertyName;
        return this;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getDescription() {
        return description;
    }

    public RzecApplicationProperties description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public RzecApplicationProperties value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RzecApplicationProperties rzecApplicationProperties = (RzecApplicationProperties) o;
        if (rzecApplicationProperties.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rzecApplicationProperties.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RzecApplicationProperties{" +
            "id=" + getId() +
            ", propertyName='" + getPropertyName() + "'" +
            ", description='" + getDescription() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }
}

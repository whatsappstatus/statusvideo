import { BaseEntity } from './../../shared';

export class Filters implements BaseEntity {
    constructor(
        public id?: string,
        public genre?: string,
        public tag?: string,
    ) {
    }
}

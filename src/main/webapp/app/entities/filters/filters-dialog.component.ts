import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Filters } from './filters.model';
import { FiltersPopupService } from './filters-popup.service';
import { FiltersService } from './filters.service';

@Component({
    selector: 'jhi-filters-dialog',
    templateUrl: './filters-dialog.component.html'
})
export class FiltersDialogComponent implements OnInit {

    filters: Filters;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private filtersService: FiltersService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.filters.id !== undefined) {
            this.subscribeToSaveResponse(
                this.filtersService.update(this.filters));
        } else {
            this.subscribeToSaveResponse(
                this.filtersService.create(this.filters));
        }
    }

    private subscribeToSaveResponse(result: Observable<Filters>) {
        result.subscribe((res: Filters) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Filters) {
        this.eventManager.broadcast({ name: 'filtersListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-filters-popup',
    template: ''
})
export class FiltersPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private filtersPopupService: FiltersPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.filtersPopupService
                    .open(FiltersDialogComponent as Component, params['id']);
            } else {
                this.filtersPopupService
                    .open(FiltersDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

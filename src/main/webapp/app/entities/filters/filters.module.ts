import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StatusVideoSharedModule } from '../../shared';
import {
    FiltersService,
    FiltersPopupService,
    FiltersComponent,
    FiltersDetailComponent,
    FiltersDialogComponent,
    FiltersPopupComponent,
    FiltersDeletePopupComponent,
    FiltersDeleteDialogComponent,
    filtersRoute,
    filtersPopupRoute,
    FiltersResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...filtersRoute,
    ...filtersPopupRoute,
];

@NgModule({
    imports: [
        StatusVideoSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        FiltersComponent,
        FiltersDetailComponent,
        FiltersDialogComponent,
        FiltersDeleteDialogComponent,
        FiltersPopupComponent,
        FiltersDeletePopupComponent,
    ],
    entryComponents: [
        FiltersComponent,
        FiltersDialogComponent,
        FiltersPopupComponent,
        FiltersDeleteDialogComponent,
        FiltersDeletePopupComponent,
    ],
    providers: [
        FiltersService,
        FiltersPopupService,
        FiltersResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StatusVideoFiltersModule {}

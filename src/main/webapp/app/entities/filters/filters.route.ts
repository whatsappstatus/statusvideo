import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { FiltersComponent } from './filters.component';
import { FiltersDetailComponent } from './filters-detail.component';
import { FiltersPopupComponent } from './filters-dialog.component';
import { FiltersDeletePopupComponent } from './filters-delete-dialog.component';

@Injectable()
export class FiltersResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const filtersRoute: Routes = [
    {
        path: 'filters',
        component: FiltersComponent,
        resolve: {
            'pagingParams': FiltersResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.filters.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'filters/:id',
        component: FiltersDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.filters.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const filtersPopupRoute: Routes = [
    {
        path: 'filters-new',
        component: FiltersPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.filters.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'filters/:id/edit',
        component: FiltersPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.filters.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'filters/:id/delete',
        component: FiltersDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.filters.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Filters } from './filters.model';
import { FiltersPopupService } from './filters-popup.service';
import { FiltersService } from './filters.service';

@Component({
    selector: 'jhi-filters-delete-dialog',
    templateUrl: './filters-delete-dialog.component.html'
})
export class FiltersDeleteDialogComponent {

    filters: Filters;

    constructor(
        private filtersService: FiltersService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.filtersService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'filtersListModification',
                content: 'Deleted an filters'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-filters-delete-popup',
    template: ''
})
export class FiltersDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private filtersPopupService: FiltersPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.filtersPopupService
                .open(FiltersDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

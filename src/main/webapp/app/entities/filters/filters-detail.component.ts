import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Filters } from './filters.model';
import { FiltersService } from './filters.service';

@Component({
    selector: 'jhi-filters-detail',
    templateUrl: './filters-detail.component.html'
})
export class FiltersDetailComponent implements OnInit, OnDestroy {

    filters: Filters;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private filtersService: FiltersService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInFilters();
    }

    load(id) {
        this.filtersService.find(id).subscribe((filters) => {
            this.filters = filters;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInFilters() {
        this.eventSubscriber = this.eventManager.subscribe(
            'filtersListModification',
            (response) => this.load(this.filters.id)
        );
    }
}

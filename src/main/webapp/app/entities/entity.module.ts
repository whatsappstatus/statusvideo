import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { StatusVideoVideosMySuffixModule } from './videos/videos-my-suffix.module';
import { StatusVideoFiltersModule } from './filters/filters.module';
import { StatusVideoRzecApplicationPropertiesModule } from './rzec-application-properties/rzec-application-properties.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        StatusVideoVideosMySuffixModule,
        StatusVideoFiltersModule,
        StatusVideoRzecApplicationPropertiesModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StatusVideoEntityModule {}

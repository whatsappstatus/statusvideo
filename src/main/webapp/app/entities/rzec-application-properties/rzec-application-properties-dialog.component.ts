import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RzecApplicationProperties } from './rzec-application-properties.model';
import { RzecApplicationPropertiesPopupService } from './rzec-application-properties-popup.service';
import { RzecApplicationPropertiesService } from './rzec-application-properties.service';

@Component({
    selector: 'jhi-rzec-application-properties-dialog',
    templateUrl: './rzec-application-properties-dialog.component.html'
})
export class RzecApplicationPropertiesDialogComponent implements OnInit {

    rzecApplicationProperties: RzecApplicationProperties;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private rzecApplicationPropertiesService: RzecApplicationPropertiesService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.rzecApplicationProperties.id !== undefined) {
            this.subscribeToSaveResponse(
                this.rzecApplicationPropertiesService.update(this.rzecApplicationProperties));
        } else {
            this.subscribeToSaveResponse(
                this.rzecApplicationPropertiesService.create(this.rzecApplicationProperties));
        }
    }

    private subscribeToSaveResponse(result: Observable<RzecApplicationProperties>) {
        result.subscribe((res: RzecApplicationProperties) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: RzecApplicationProperties) {
        this.eventManager.broadcast({ name: 'rzecApplicationPropertiesListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-rzec-application-properties-popup',
    template: ''
})
export class RzecApplicationPropertiesPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rzecApplicationPropertiesPopupService: RzecApplicationPropertiesPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.rzecApplicationPropertiesPopupService
                    .open(RzecApplicationPropertiesDialogComponent as Component, params['id']);
            } else {
                this.rzecApplicationPropertiesPopupService
                    .open(RzecApplicationPropertiesDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RzecApplicationProperties } from './rzec-application-properties.model';
import { RzecApplicationPropertiesPopupService } from './rzec-application-properties-popup.service';
import { RzecApplicationPropertiesService } from './rzec-application-properties.service';

@Component({
    selector: 'jhi-rzec-application-properties-delete-dialog',
    templateUrl: './rzec-application-properties-delete-dialog.component.html'
})
export class RzecApplicationPropertiesDeleteDialogComponent {

    rzecApplicationProperties: RzecApplicationProperties;

    constructor(
        private rzecApplicationPropertiesService: RzecApplicationPropertiesService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.rzecApplicationPropertiesService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'rzecApplicationPropertiesListModification',
                content: 'Deleted an rzecApplicationProperties'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-rzec-application-properties-delete-popup',
    template: ''
})
export class RzecApplicationPropertiesDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rzecApplicationPropertiesPopupService: RzecApplicationPropertiesPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.rzecApplicationPropertiesPopupService
                .open(RzecApplicationPropertiesDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { RzecApplicationProperties } from './rzec-application-properties.model';
import { RzecApplicationPropertiesService } from './rzec-application-properties.service';

@Component({
    selector: 'jhi-rzec-application-properties-detail',
    templateUrl: './rzec-application-properties-detail.component.html'
})
export class RzecApplicationPropertiesDetailComponent implements OnInit, OnDestroy {

    rzecApplicationProperties: RzecApplicationProperties;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private rzecApplicationPropertiesService: RzecApplicationPropertiesService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRzecApplicationProperties();
    }

    load(id) {
        this.rzecApplicationPropertiesService.find(id).subscribe((rzecApplicationProperties) => {
            this.rzecApplicationProperties = rzecApplicationProperties;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRzecApplicationProperties() {
        this.eventSubscriber = this.eventManager.subscribe(
            'rzecApplicationPropertiesListModification',
            (response) => this.load(this.rzecApplicationProperties.id)
        );
    }
}

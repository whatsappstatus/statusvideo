import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RzecApplicationPropertiesComponent } from './rzec-application-properties.component';
import { RzecApplicationPropertiesDetailComponent } from './rzec-application-properties-detail.component';
import { RzecApplicationPropertiesPopupComponent } from './rzec-application-properties-dialog.component';
import { RzecApplicationPropertiesDeletePopupComponent } from './rzec-application-properties-delete-dialog.component';

@Injectable()
export class RzecApplicationPropertiesResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const rzecApplicationPropertiesRoute: Routes = [
    {
        path: 'rzec-application-properties',
        component: RzecApplicationPropertiesComponent,
        resolve: {
            'pagingParams': RzecApplicationPropertiesResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.rzecApplicationProperties.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'rzec-application-properties/:id',
        component: RzecApplicationPropertiesDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.rzecApplicationProperties.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const rzecApplicationPropertiesPopupRoute: Routes = [
    {
        path: 'rzec-application-properties-new',
        component: RzecApplicationPropertiesPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.rzecApplicationProperties.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rzec-application-properties/:id/edit',
        component: RzecApplicationPropertiesPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.rzecApplicationProperties.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rzec-application-properties/:id/delete',
        component: RzecApplicationPropertiesDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.rzecApplicationProperties.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { RzecApplicationProperties } from './rzec-application-properties.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RzecApplicationPropertiesService {

    private resourceUrl = SERVER_API_URL + 'api/rzec-application-properties';

    constructor(private http: Http) { }

    create(rzecApplicationProperties: RzecApplicationProperties): Observable<RzecApplicationProperties> {
        const copy = this.convert(rzecApplicationProperties);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(rzecApplicationProperties: RzecApplicationProperties): Observable<RzecApplicationProperties> {
        const copy = this.convert(rzecApplicationProperties);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<RzecApplicationProperties> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RzecApplicationProperties.
     */
    private convertItemFromServer(json: any): RzecApplicationProperties {
        const entity: RzecApplicationProperties = Object.assign(new RzecApplicationProperties(), json);
        return entity;
    }

    /**
     * Convert a RzecApplicationProperties to a JSON which can be sent to the server.
     */
    private convert(rzecApplicationProperties: RzecApplicationProperties): RzecApplicationProperties {
        const copy: RzecApplicationProperties = Object.assign({}, rzecApplicationProperties);
        return copy;
    }
}

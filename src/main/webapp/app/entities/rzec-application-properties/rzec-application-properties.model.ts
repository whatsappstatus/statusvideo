import { BaseEntity } from './../../shared';

export class RzecApplicationProperties implements BaseEntity {
    constructor(
        public id?: string,
        public propertyName?: string,
        public description?: string,
        public value?: string,
    ) {
    }
}

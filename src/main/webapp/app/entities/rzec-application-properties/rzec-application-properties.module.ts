import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StatusVideoSharedModule } from '../../shared';
import {
    RzecApplicationPropertiesService,
    RzecApplicationPropertiesPopupService,
    RzecApplicationPropertiesComponent,
    RzecApplicationPropertiesDetailComponent,
    RzecApplicationPropertiesDialogComponent,
    RzecApplicationPropertiesPopupComponent,
    RzecApplicationPropertiesDeletePopupComponent,
    RzecApplicationPropertiesDeleteDialogComponent,
    rzecApplicationPropertiesRoute,
    rzecApplicationPropertiesPopupRoute,
    RzecApplicationPropertiesResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...rzecApplicationPropertiesRoute,
    ...rzecApplicationPropertiesPopupRoute,
];

@NgModule({
    imports: [
        StatusVideoSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RzecApplicationPropertiesComponent,
        RzecApplicationPropertiesDetailComponent,
        RzecApplicationPropertiesDialogComponent,
        RzecApplicationPropertiesDeleteDialogComponent,
        RzecApplicationPropertiesPopupComponent,
        RzecApplicationPropertiesDeletePopupComponent,
    ],
    entryComponents: [
        RzecApplicationPropertiesComponent,
        RzecApplicationPropertiesDialogComponent,
        RzecApplicationPropertiesPopupComponent,
        RzecApplicationPropertiesDeleteDialogComponent,
        RzecApplicationPropertiesDeletePopupComponent,
    ],
    providers: [
        RzecApplicationPropertiesService,
        RzecApplicationPropertiesPopupService,
        RzecApplicationPropertiesResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StatusVideoRzecApplicationPropertiesModule {}

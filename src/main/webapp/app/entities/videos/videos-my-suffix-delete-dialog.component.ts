import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VideosMySuffix } from './videos-my-suffix.model';
import { VideosMySuffixPopupService } from './videos-my-suffix-popup.service';
import { VideosMySuffixService } from './videos-my-suffix.service';

@Component({
    selector: 'jhi-videos-my-suffix-delete-dialog',
    templateUrl: './videos-my-suffix-delete-dialog.component.html'
})
export class VideosMySuffixDeleteDialogComponent {

    videos: VideosMySuffix;

    constructor(
        private videosService: VideosMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.videosService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'videosListModification',
                content: 'Deleted an videos'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-videos-my-suffix-delete-popup',
    template: ''
})
export class VideosMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private videosPopupService: VideosMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.videosPopupService
                .open(VideosMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

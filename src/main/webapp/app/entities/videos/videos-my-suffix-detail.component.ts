import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { VideosMySuffix } from './videos-my-suffix.model';
import { VideosMySuffixService } from './videos-my-suffix.service';

@Component({
    selector: 'jhi-videos-my-suffix-detail',
    templateUrl: './videos-my-suffix-detail.component.html'
})
export class VideosMySuffixDetailComponent implements OnInit, OnDestroy {

    videos: VideosMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private videosService: VideosMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVideos();
    }

    load(id) {
        this.videosService.find(id).subscribe((videos) => {
            this.videos = videos;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVideos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'videosListModification',
            (response) => this.load(this.videos.id)
        );
    }
}

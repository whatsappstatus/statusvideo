import { BaseEntity } from './../../shared';

export const enum Language {
    'TAMIL',
    'HINDI',
    'TELUGU'
}

export class VideosMySuffix implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string,
        public url?: string,
        public views?: number,
        public tag?: string,
        public actor?: string,
        public createdDate?: any,
        public genre?: string,
        public language?: Language,
        public credits?: string,
        public image_url?: string,
        public movie_name?: string,
    ) {
    }
}

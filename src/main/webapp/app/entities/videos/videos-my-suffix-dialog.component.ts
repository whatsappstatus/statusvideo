import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VideosMySuffix } from './videos-my-suffix.model';
import { VideosMySuffixPopupService } from './videos-my-suffix-popup.service';
import { VideosMySuffixService } from './videos-my-suffix.service';

@Component({
    selector: 'jhi-videos-my-suffix-dialog',
    templateUrl: './videos-my-suffix-dialog.component.html'
})
export class VideosMySuffixDialogComponent implements OnInit {

    videos: VideosMySuffix;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private videosService: VideosMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.videos.id !== undefined) {
            this.subscribeToSaveResponse(
                this.videosService.update(this.videos));
        } else {
            this.subscribeToSaveResponse(
                this.videosService.create(this.videos));
        }
    }

    private subscribeToSaveResponse(result: Observable<VideosMySuffix>) {
        result.subscribe((res: VideosMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: VideosMySuffix) {
        this.eventManager.broadcast({ name: 'videosListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-videos-my-suffix-popup',
    template: ''
})
export class VideosMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private videosPopupService: VideosMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.videosPopupService
                    .open(VideosMySuffixDialogComponent as Component, params['id']);
            } else {
                this.videosPopupService
                    .open(VideosMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StatusVideoSharedModule } from '../../shared';
import {
    VideosMySuffixService,
    VideosMySuffixPopupService,
    VideosMySuffixComponent,
    VideosMySuffixDetailComponent,
    VideosMySuffixDialogComponent,
    VideosMySuffixPopupComponent,
    VideosMySuffixDeletePopupComponent,
    VideosMySuffixDeleteDialogComponent,
    videosRoute,
    videosPopupRoute,
    VideosMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...videosRoute,
    ...videosPopupRoute,
];

@NgModule({
    imports: [
        StatusVideoSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        VideosMySuffixComponent,
        VideosMySuffixDetailComponent,
        VideosMySuffixDialogComponent,
        VideosMySuffixDeleteDialogComponent,
        VideosMySuffixPopupComponent,
        VideosMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        VideosMySuffixComponent,
        VideosMySuffixDialogComponent,
        VideosMySuffixPopupComponent,
        VideosMySuffixDeleteDialogComponent,
        VideosMySuffixDeletePopupComponent,
    ],
    providers: [
        VideosMySuffixService,
        VideosMySuffixPopupService,
        VideosMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StatusVideoVideosMySuffixModule {}

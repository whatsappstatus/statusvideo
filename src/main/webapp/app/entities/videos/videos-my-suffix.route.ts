import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { VideosMySuffixComponent } from './videos-my-suffix.component';
import { VideosMySuffixDetailComponent } from './videos-my-suffix-detail.component';
import { VideosMySuffixPopupComponent } from './videos-my-suffix-dialog.component';
import { VideosMySuffixDeletePopupComponent } from './videos-my-suffix-delete-dialog.component';

@Injectable()
export class VideosMySuffixResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const videosRoute: Routes = [
    {
        path: 'videos-my-suffix',
        component: VideosMySuffixComponent,
        resolve: {
            'pagingParams': VideosMySuffixResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.videos.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'videos-my-suffix/:id',
        component: VideosMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.videos.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const videosPopupRoute: Routes = [
    {
        path: 'videos-my-suffix-new',
        component: VideosMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.videos.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'videos-my-suffix/:id/edit',
        component: VideosMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.videos.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'videos-my-suffix/:id/delete',
        component: VideosMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'statusVideoApp.videos.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
